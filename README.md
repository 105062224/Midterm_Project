# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name] Chat-Chat
* Key functions (add/delete)
    1. 公共聊天室
    2. 一對一私人聊天室
    3. 訊息通知(公共&私人)
* Other functions (add/delete)
    1. 設置個人檔案、檢視他人個人檔案
    2. 搜尋用戶
    3. 自己的留言紀錄
    4. 私人聊天室的連結清單
## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
  作品網址 : [https://project01-c36ba.firebaseapp.com/]
  報告網址 : [https://105062224.gitlab.io/Midterm_Project/README.md]

  重點功能：
1. 左上方的Nav bar控制登入、登出、移動到其他頁面的功能。
2. 所有的留言都會顯示用戶名，時間，留言內容。
3. 附有搜尋其他用戶的功能。
4. 能夠設立並更改個人資料供其他用戶使用。
5. 有私人1-on-1聊天與公共聊天兩種聊天方式。
6. 能夠記錄自己在公共聊天室的留言紀錄。
7. 會出現與用戶私人對話過的用戶列表。
8. 當私人留言或公共聊天室有人留言是會出現通知，按下通知可以轉跳到相應的聊天室
9. CSS animation為背景更改顏色的動畫。


index(首頁)：
1. 中間處顯示個人資料，可按set按鈕更改個人資料(進入profile.html)
2. 右上search功能，輸入其他用戶的email or name可以尋找其他用戶並觀看其個人資料(進入visit.html)
3. 左下方"Your Record at Public"能夠顯示你在public room所留下的所有留言
4. 中下方的按鈕能夠進入public room(進入page.html)
5. 右下方為"Private Chat"會顯示與用戶進行過私人聊天的人名，點選他們可以進入與他們的私人聊天室(進入chat.html)

profile(個人資料)
能夠更改用戶的各項資料，如果不想讓別人知道可以將欄位留白，閱讀資訊會自行改為保密

public room(公共聊天室)
1. 所有用戶都可在此處留言，留言紀錄也會顯示在首頁的"Your Record at Public"
2. 背景為可變色的css animation
3. 可以點擊除了自己以外的用戶的用戶名稱進入到那個人的個人資料瀏覽(進入visit.html)
4. 如果第一次進入此聊天室且尚未設定個人資料，會跳出警告並轉跳到個人資料更改的頁面(進入 profile.html)
5. 若有人在此處留言會跳出轉跳到此聊天室的留言

visit(個人資料瀏覽)
1. 可以藉由首頁search或在public room點擊他人名稱進入此處
2. 顯示該用戶的個人資料
3. 能夠點選"chat"進入私人聊天室與其他人私聊(進入chat.html)
4. 點擊"back"返回首頁

private chat(私人聊天)
1. 只有兩個人能夠進入同一個私人聊天室進行
2. 背景為css animation
3. 若有人私訊用湖則會跳出私人訊息的通知，按下可以進行轉跳

  
## Security Report (Optional)
藉由認證使用者達到防止他人更改用戶的個人資料跟防止第三人闖入兩人的私人聊天室的功能



