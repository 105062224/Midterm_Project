function init(){
    firebase.auth().onAuthStateChanged(function (user) {
        var information = document.getElementById('information');
        var menu = document.getElementById('dynamic-menu');
        var change_information = document.getElementById('change_information');
        var room1 = document.getElementById('go_room1');
        if (user) {
            change_information.innerHTML = "Set";
            change_information.addEventListener('click',function(){
                window.location.href = "profile.html";
            })
            
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                }).then(window.location.href = "index.html").catch(function(e)
                {
                  create_alert("error",e.message);
                });
            })
            room1.addEventListener('click',function(){
                window.location.href = "page.html";
            })

            // database
            var user = firebase.auth().currentUser;

            firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot) {
                var info_name = snapshot.val().name;
                var info_school = snapshot.val().school;
                var info_phone = snapshot.val().phone;
                var info_age = snapshot.val().age;
                var info_birthday = snapshot.val().birthday;
                var info_gender = snapshot.val().gender;
                var info_city = snapshot.val().city;

                if(snapshot.val().name==""||snapshot.val().name==undefined) info_name="保密";
                if(snapshot.val().school==""||snapshot.val().school==undefined) info_school="保密";
                if(snapshot.val().phone==""||snapshot.val().phone==undefined) info_phone="保密";
                if(snapshot.val().age==""||snapshot.val().age==undefined) info_age="保密";
                if(snapshot.val().birthday==""||snapshot.val().birthday==undefined) info_birthday="保密";
                if(snapshot.val().gender==""||snapshot.val().gender==undefined) info_gender="保密";
                if(snapshot.val().city==""||snapshot.val().city==undefined) info_city="保密";
                console.log(info_school);

                information.innerHTML = "<strong> Name：</strong>" + info_name + "</br>" +
            "<strong> School：</strong>" + info_school + "</br>" + "<strong>Phone：</strong>" + info_phone + "</br>" + "<strong> Age：</strong>" +
            info_age + "</br>" + "<strong>Birthday：</strong>" + info_birthday + "</br>" + "<strong> Gender：</strong>" + info_gender + "</br>" +
            "<strong> City：</strong>" + info_city  ;
            });


                //get history_comment
            var history_comment = document.getElementById('history_comment');
            history_comment.setAttribute("style","overflow:scroll ; height:356px");

            var str_before_username = "<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
            var str_after_content = "</p></div></div>\n";

            var postsRef = firebase.database().ref('com_list');    
            //var total_post0 = [];
            var str0="";

            postsRef.once('value').then(function (snapshot) {
                snapshot.forEach(function(childshot){
                    var data = childshot.val();
                    if(data.uid==user.uid){
                        str0 = str0 + str_before_username + "<span  style='font-size:15px'>" + data.data + "</span></br><span> (" + data.time + ")</span>" + str_after_content;
                        history_comment.innerHTML = str0;
                    }
                //total_post[total_post.length] =str_before_username + data + str_after_content;
                });
            //history_comment.innerHTML = total_post.join('');
            })
            .catch(e => console.log(e.message));


            var str_before_username0 = "<div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'>";
            var str_after_content0 = "</p></div></div>\n";

            //get chat_list
            var chat_list = document.getElementById('chat_list');
            chat_list.setAttribute("style","overflow:scroll ; height:356px");

            var chatRef = firebase.database().ref('chat_room');    
            //var total_post = [];
            var str1="";
            var uid;
            var chat;

            chatRef.once('value').then(function (snapshot) {
                snapshot.forEach(function(childshot){
                    var data = childshot.val();
                    if(data.uid1==user.uid){
                        str1 = str1 + str_before_username + "<span style='font-size:15px' ><a href='chat.html?"  + data.uid2 + "'>" + data.name2 + "</a></span>" + str_after_content;
                        chat_list.innerHTML = str1;
                    }
                    else if(data.uid2==user.uid){
                        str1 = str1 + str_before_username + "<span  style='font-size:15px'><a href='chat.html?"  + data.uid1 + "'>" + data.name1 + "</a></span>" + str_after_content +  "</a>";
                        chat_list.innerHTML = str1;
                    }
                //total_post[total_post.length] =str_before_username + data + str_after_content;
                });
            //history_comment.innerHTML = total_post.join('');
            })
            .catch(e => console.log(e.message));

            //search
            var input = document.getElementById('input');
            var search = document.getElementById('search');
            search.addEventListener('click',function(){
                var usersRef = firebase.database().ref('users');
                usersRef.once('value').then(function (snapshot) {
                    snapshot.forEach(function(childshot){
                        var data = childshot.val();
                        if((data.name==input.value || data.email==input.value) && data.email != user.email && input.value!=""){
                            window.location.href = "visit.html?" + data.uid ;
                        }
                    });
                })
            })



        } else {
            information.innerHTML = "Please Login";
            change_information.innerHTML = "Login";
            change_information.addEventListener('click',function(){
                window.location.href = "signin.html";
            })
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            //document.getElementById('post_list').innerHTML = "";
            room1.addEventListener('click',function(){
                window.location.href = "signin.html";
            })
        }
    });

    //ANIMATION
    

}




window.onload = function () {
    init();
};