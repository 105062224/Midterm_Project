function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var postsRef = firebase.database().ref('com_list');
      var first_count=0;
      var second_count=0;
      postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childshot){
                first_count += 1
            });
            postsRef.on('child_added', function (data) {
                second_count += 1;
                var childData = data.val();
                if (second_count > first_count) {
                  var notification = new Notification('Public Message', {
                    icon: 'img/house.png',
                    body:"From\t" + childData.name ,
                  });
                  notification.onclick = function () {
                    window.location.href = "page.html";      
                  };
                }
            });
        })
  
  
    }
}

notifyMe();